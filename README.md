## Sketcher Application for Node.js/React & Development Environments

This is a repository for Node.js/React & Development Environments electives at Web Development INT KEA.
The project is created and maintained by Anastazja Galuza, Magnus Vagn Jensen & Jakob Falkenberg Hansen.

## DOCS

https://magn3506.gitlab.io/sketcher_app_react_devenv_exam_repo/

## Git-hooks implementation tutorial

https://medium.com/better-programming/simple-git-hooks-with-create-react-app-eslint-and-husky-6983806dba5c

## QA test site

https://sketcher-app-qa.herokuapp.com/
