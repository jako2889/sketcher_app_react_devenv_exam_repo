// ** Guide for using mongodb in node: https://www.w3schools.com/nodejs/nodejs_mongodb.asp
// Require MongoClient to later instantiate a client with our connection uri
const { MongoClient } = require('mongodb');

// Require dotenv configuration for passing string values of connection uri from .env
require('dotenv').config();

// Declare a connection uri const with the value from .env
const connectionURI = process.env.CONNECTION_URI;
// const connectionURI = 'mongodb+srv://admin_user:4cNHIGVTA1irl90s@sketcher.t3psu.mongodb.net/sketcherDB?retryWrites=true&w=majority';

// Instantiate the client with MongoClient,
// which takes the connection uri and a special parameter of useUnifiedTopology set to true
// for reference: https://mongodb.github.io/node-mongodb-native/3.3/reference/unified-topology/
const client = new MongoClient(connectionURI, { useUnifiedTopology: true });

// use the connect callback to connect
client.connect((error) => {
  if (!error) {
    // Set every email field to be unique
    client.db('sketcherDB').collection('users').createIndex({ email: 1 }, { unique: true });
    return 'Connection succesful';
  }
  return error;
});

// !! important to export the client for later use in routes
module.exports = client;
