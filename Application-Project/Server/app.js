// Require express
const express = require('express');
// Create a express application called app
const app = express();
const path = require('path');

const PORT = process.env.PORT || 8080;

// Use json and urlencoded test
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// ENV
require('dotenv').config();

// Search static files
app.use(express.static(path.join(path.resolve(__dirname, '..'), 'client/build')));

// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PATCH, POST, GET, DELETE');
  next();
});

// ROUTESS
const drawingsRoute = require('./routes/drawings');
// Tell express to use route
app.use(drawingsRoute);

app.get('*', (req, res) => {
  res.sendFile(path.join(path.resolve(__dirname, '..'), '/client/build/index.html'));
});

// Method that needs a port number and returns a node server
app.listen(PORT, (error) => {
  if (error) {
    console.log(`There is an error running on the server ${error}`);
  }
  console.log(`Server is running on port ${PORT}`);
});
