import React, { Component } from 'react';

class DrawingList extends Component {
  constructor(props) {
    super(props);
    this.state = { drawings: null, isLoading: true };
  }

  componentDidMount() {
    fetch('/drawings')
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.setState({ drawings: data, isLoading: false });
      });
  }

  render() {
    const { drawings, isLoading } = this.state;
    return (
      <div>
        {isLoading === false ? drawings.map((drawing) => (
          <div key={drawing}>
            <p>{drawing.drawing}</p>
          </div>
        )) : <p>Loading..</p>}
      </div>
    );
  }
}

export default DrawingList;
