// Reference guide to testing: https://www.smashingmagazine.com/2020/06/practical-guide-testing-react-applications-jest/
import App from './App';

// It is a method where you describe the test and pass functions to run while testing
// fx were passing the shallow method
it('renders without crashing', () => {
  // This takes a snapshot of the code and adds the snapshot to a folder in src
  // Reference please read: https://jestjs.io/docs/en/snapshot-testing
  expect(App).toMatchSnapshot();
});
